Source: node-yjs
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Roland Mas <lolando@debian.org>
Testsuite: autopkgtest-pkg-nodejs
Build-Depends:
 debhelper-compat (= 13)
 , dh-sequence-nodejs
 , rollup
 , node-rollup-plugin-node-resolve
 , node-rollup-plugin-commonjs
 , node-lib0
 , node-typescript
Standards-Version: 4.6.0
Homepage: https://docs.yjs.dev
Vcs-Git: https://salsa.debian.org/js-team/node-yjs.git
Vcs-Browser: https://salsa.debian.org/js-team/node-yjs
Rules-Requires-Root: no

Package: node-yjs
Architecture: all
Depends:
 ${misc:Depends}
 , node-lib0 (>= 0.2.43)
Description: CRDT framework with a powerful abstraction of shared data
 Yjs is a conflict-free replicated data type (CRDT) implementation
 that exposes its internal data structure as shared types. Shared
 types are common data types like Map or Array with superpowers:
 changes are automatically distributed to other peers and merged
 without merge conflicts.
 .
 Yjs is network agnostic (p2p!), supports many existing rich text
 editors, offline editing, version snapshots, undo/redo and shared
 cursors. It scales well with an unlimited number of users and is well
 suited for even large documents.
 .
 Node.js is an event-based server-side JavaScript engine.
